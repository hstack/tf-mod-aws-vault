variable "name"                 { default = "vault" }
variable "hstack_version"       { default = "1.0" }
variable "atlas_username"       { default = "hstack" }
variable "region"               { default = "us-east-1" }

variable "domain"               { default = "consul" }
variable "dc"                   { default = "dc1" }

variable "root_ca"              { default = "" }
variable "subnet_ids"           { }
variable "keypair_name"         { }

variable "count"                { default = "-1" }

variable "instance_type"        { default = "t2.medium" }

variable "iam_instance_profile"  { default = "consul_node" }

variable "encrypt_key"          { }
variable "join_ipv4_addr"       { default = "" }

variable "autojoin_tag_value"   { default = "" }
variable "autojoin_tag_key"     { default = "consul_autojoin" }

variable "keybase_handles"      { }
variable "key_threshold"        { default = "3" }
