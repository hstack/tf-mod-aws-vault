#--------------------------------------------------------------
# This module creates all resources necessary for Vault
#--------------------------------------------------------------
data "atlas_artifact" "vault_ami" {
  name = "${var.atlas_username}/aws-${var.region}-coreos-hstack"

  type = "amazon.image"
  metadata {
     version = "${var.hstack_version}"
  }
}

data "aws_subnet" "selected" {
  id = "${element(split(",",var.subnet_ids), 0)}"
}

data "aws_vpc" "selected" {
  id = "${data.aws_subnet.selected.vpc_id}"
}

resource "aws_security_group" "vault_servers" {
  count =  "${var.count> 0 ? 1 : 0}"

  name        = "${var.name}"
  vpc_id      = "${data.aws_vpc.selected.id}"
  description = "Security group for Vault"

  tags      { Name = "${var.name}" }
  lifecycle { create_before_destroy = true }

  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["${data.aws_vpc.selected.cidr_block}"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Vault is the source of TLS certificates but vault will use consul
# as its storage backend.
#
# Chicken||Egg syndrom!
#
# Consul agent & servers has to be bootstrapped before vault & without SSL enabled
# Then, when vault is unsealed and active, each consul
# server will get its own private cert from vault and reload its configuration
data "template_file" "vault_user_data" {
  template = "${file("${path.module}/vault.tpl")}"

  vars {
    join_ipv4_addr        = "${var.join_ipv4_addr}"
    private_network       = "${data.aws_vpc.selected.cidr_block}"
    autojoin_tag_key      = "${var.autojoin_tag_key}"
    autojoin_tag_value    = "${coalesce(var.autojoin_tag_value, var.name)}"
    root_ca               = "${base64encode(var.root_ca)}"
    domain                = "${var.domain}"
    dc                    = "${var.dc}"
    encrypt_key           = "${var.encrypt_key}"
    keybase_handles       = "${var.keybase_handles}"
    key_threshold         = "${var.key_threshold}"
  }
}

resource "aws_launch_configuration" "vault_servers" {
  count =  "${var.count> 0 ? 1 : 0}"

  name_prefix = "${var.name}"
  iam_instance_profile = "${var.iam_instance_profile}"
  image_id    = "${lookup(data.atlas_artifact.vault_ami.metadata_full, format("region-%s", var.region))}"
  instance_type = "${var.instance_type}"
  security_groups = ["${aws_security_group.vault_servers.id}"]

  associate_public_ip_address = "false"
  key_name                = "${var.keypair_name}"

  user_data     = "${data.template_file.vault_user_data.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}

//  Auto-scaling group for our cluster.
resource "aws_autoscaling_group" "vault_servers" {
  count =  "${var.count> 0 ? 1 : 0}"

  name_prefix = "${var.name}"
  launch_configuration = "${aws_launch_configuration.vault_servers.name}"
  desired_capacity     = "${var.count}"
  min_size             = "${var.count}"
  max_size             = "${var.count}"
  health_check_grace_period = "60"
  health_check_type         = "EC2"
  force_delete              = false
  wait_for_capacity_timeout = 0

  vpc_zone_identifier = [ "${split(",",var.subnet_ids)}"]

  enabled_metrics           = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances"
  ]

  tag {
    key                 = "Name"
    value               = "${var.name}"
    propagate_at_launch = true
  }

  tag {
    key                 = "${var.autojoin_tag_key}"
    value               = "${coalesce(var.autojoin_tag_value, var.name)}"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}
