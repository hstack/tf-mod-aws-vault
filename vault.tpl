#cloud-config
write_files:
  - path: "/etc/hstack/hstack.conf"
    permissions: "0644"
    owner: "root"
    content: | 
      PRIVATE_NETWORK="${ private_network }"
      CONSUL_MODE="agent"
      CONSUL_AUTOJOIN="aws"
      CONSUL_AUTOJOIN_AWS_TAG_VALUE="${ autojoin_tag_value }"
      CONSUL_AUTOJOIN_AWS_TAG_KEY="${ autojoin_tag_key }"
      CONSUL_AGENT_TAGS="vault-server,${ autojoin_tag_value }"
      DOMAIN=${ domain }
      DATACENTER=${ dc }
      CONSUL_ENCRYPT_KEY=${ encrypt_key }
      JOIN_IPV4_ADDR="${ join_ipv4_addr }"
      VAULT_MODE=server
      VAULT_PGP_KEYS="${ keybase_handles }"
      VAULT_KEY_THRESHOLD=${ key_threshold }
  - path: "/etc/hstack/certs/ca.pem"
    permissions: "0644"
    owner: "root"
    encoding: base64
    content: ${root_ca}
